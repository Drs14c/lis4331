> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4331

## DeVon Singleton


### Project 1 requirements:			     |#### README.md file should include the following items  |#### Git commands w/short descriptions:
-----------------------------------------------------|:------------------------------------------------------:|---------------------------------------
1. Distributed Version Control with Git and bitbucket|* Screenshot of Music Player - Splash Screen            |1. git init - create a new local repository
						     |* Screenshot of Music Player pause                      |2. git status - list the files you have changed and those you still need to add or commit
2. Develop Music Player 		             |* Screenshot of Music Player play	           	      |3. git add - add one or more files to staging(index)
						     |							      |4. git commit - commit changes to head (but not yet the remote repoistory) 
3. Develop a Splash Screen			     |							      |5. git diff - view all the merge conflicts
						     |							      |6. git push - send changes to the master branch of your remote repository						  	 
4. Use a launcher icon				     |                                                        |7. git pull - fetch and merged changes on the remote server to your working directory    
						     |                                                        |
5. Use a toast notification for out of range values  |							      |
						     |						 	      |
						     |							      |
						     |							      |
						     |				       			      |

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 



#### Assignment Screenshots:

*Screenshot of Splash Screen*![Home Screenshot](img/splash.png)

*Screenshot of Music Player : play song*:![ Screenshot](img/play.png)

*Screenshot of Music Player : pause song *:![ Screenshot](img/pause.png)







#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/ "Bitbucket Station Locations")

