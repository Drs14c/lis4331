> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below-- otherwise, points **will** be deducted.
>

# Lis4331 Advance Mobile Application

## DeVon Singleton


*Course Work Link*




1. [A1 README.md](a1/README.md)

	- Install Java
	
	- Install Android Studio

	- Set up version control : BitBucket

	- Provide git command descriptions

2.  [A2 README.md](a2/README.md)
	
	- Develop Tip Calculator
	
	- Use drop down menu for tip percentage and guest number

	

3. [A3 README.md](a3/README.md)
	
	- Develop Currency Converter
	
	- Deploy a launcher icon

	- Use toast notification for out of range values 

4. [A4 README.md](a4/README.md)

	- Develop Home Mortgage Interest Calculator

	- Utilize a splash screen

	- Deploy a launcher icon

	

5. [A5 README.md](a5/README.md)
	
	- Develop News Reader

	- Utilize splash screen

	- Use RSS feed as source for articles 

6. [P1 README.md](p1/README.md)

	- Develop Music Player

	- Develop Splash Screen 

7. [P2 README.md](p2/README.md)

	- Develop Task List

	- Develop Splash Screen


