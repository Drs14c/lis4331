> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4331

## DeVon Singleton


###Asssignment 4 requirements:			     |####README.md file should include the following items   |####Git commands w/short descriptions:								|###Screenshots
-----------------------------------------------------|:------------------------------------------------------:|---------------------------------------								|---------------------------
1. Distributed Version Control with Git and bitbucket|* Screenshot of  - Mortgage Interest Calculator         |1. git init - create a new local repository							|*Screenshot of Mortgage Calculator*![Home Screenshot](img/splash.png)
						     |* Screenshot of Mortgage Interest Calculator populated  |2. git status - list the files you have changed and those you still need to add or commit	|*Screenshot of Populated Calculator*:![ Screenshot](img/compute.png)
2. Develop Mortgage Interest Calculator	             |* Screenshot of Wrong input notification		      |3. git add - add one or more files to staging(index)						|*Screenshot of Wrong input for years*:![ Screenshot](img/error.png)
						     |							      |4. git commit - commit changes to head (but not yet the remote repoistory) 			|
3. Include Splash Screen			     |							      |5. git diff - view all the merge conflicts							|
						     |							      |6. git push - send changes to the master branch of your remote repository			|			  	 
4. Use a launcher icon				     |                                                        |7. git pull - fetch and merged changes on the remote server to your working directory    	|
						     |                                                        |
5. Use a toast notification for out of range values  |							      |
						     |						 	      |
						     |							      |
						     |							      |
						     |				       			      |

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 













#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/ "Bitbucket Station Locations")

