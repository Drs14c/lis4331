> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4331

## DeVon Singleton

### Assignment 1 Requirements:

*Course Work Link*

 [A1 README.md](a1/README.md)
    
    1. Distributed Version Control with Git and Bitbucket	
    
    2. Development Installations
    
    3. Questions 
    
    4. Provide screenshots of installation.

|#### README.md file should include the following items  |#### Git commands w/short descriptions:
|------------------------------------------------------- |---------------------------------------
|* Screenshot of Android Studio - Contact App	         |1. git init - create a new local repository
|* Screenshot of running java Hello                      |2. git status - list the files you have changed and those you still need to add or commit
|* Screenshot of Android Studio - My First App		 |3. git add - add one or more files to staging(index)
|							 |4. git commit - commit changes to head (but not yet the remote repoistory) 
|							 |5. git diff - view all the merge conflicts
|							 |6. git push - send changes to the master branch of your remote repository						  	 
|                                                        |7. git pull - fetch and merged changes on the remote server to your working directory    
|                                                        |
|							 |
|						 	 |
|							 |
|							 |
|							 |

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 



#### Assignment Screenshots:

*Screenshot of My First App*![Hello DeVon Screenshot](img/hellodevon.png)
*Screenshot of running java Hello*:![Java Hello Screenshot](img/hello.png)

*Screenshot of running Contact App*:
![Contact Home Screen Screenshot](img/home.png)
![Contact Screen Screenshot](img/contact.png)








#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/ "Bitbucket Station Locations")

